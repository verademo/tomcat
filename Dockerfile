FROM centos:7

ENV TOMCAT_VER "9.0.26"

RUN yum install -y java-11-openjdk-devel wget
RUN groupadd --system tomcat
RUN useradd -d /usr/share/tomcat -r -s /bin/false -g tomcat tomcat
RUN wget https://archive.apache.org/dist/tomcat/tomcat-9/v${TOMCAT_VER}/bin/apache-tomcat-${TOMCAT_VER}.tar.gz && \
    tar xvf apache-tomcat-${TOMCAT_VER}.tar.gz -C /usr/share/ && \
    ln -s /usr/share/apache-tomcat-${TOMCAT_VER}/ /usr/share/tomcat && \
    chown -R tomcat:tomcat /usr/share/tomcat && \
    chown -R tomcat:tomcat /usr/share/apache-tomcat-${TOMCAT_VER}/
    
EXPOSE 8080
    
CMD ["/usr/share/tomcat/bin/catalina.sh", "run"]
